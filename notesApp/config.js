import 'firebase/compat/auth'
import 'firebase/compat/firestore'
import { initializeApp } from 'firebase/app';

const firebaseConfig = {
    apiKey: "AIzaSyCoE2NelbYQ6dBA8xwsuUHioNc8Fco8SDk",
    authDomain: "notesapp-cc216.firebaseapp.com",
    projectId: "notesapp-cc216",
    storageBucket: "notesapp-cc216.appspot.com",
    messagingSenderId: "806791067159",
    appId: "1:806791067159:web:9a73cc812924457f0d8e52",
    measurementId: "G-MFBTRGMZ3W"
  };

  // Initialize Firebase
const app = initializeApp(firebaseConfig);

export default app;

