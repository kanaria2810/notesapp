import { View, Text, Button, StyleSheet } from 'react-native'
import React, { useEffect, useState } from 'react'
import { useNavigation } from '@react-navigation/native'
import app from '../config'
import { collection, getDocs } from "firebase/firestore";
import { getFirestore } from "firebase/firestore";
import { FlashList } from '@shopify/flash-list'

// Initialize Cloud Firestore and get a reference to the service
const db = getFirestore(app);

const Home = () => {

    const [notes, setNotes] = useState([]);
    const navigation = useNavigation();

    //Fetch the data from firestore
    useEffect(() => {
        (async () => {
          try {
            const newNotes = []
            const querySnapshot = await getDocs(collection(db, "notes"));
            querySnapshot.forEach((doc) => {
              const {note, title} = doc.data();
              newNotes.push({note, title, id: doc.id});
            });
            setNotes(newNotes);
          } catch (e) {
            console.error("Catch error: " +  JSON.stringify(e))
            setError(JSON.stringify(e))
          }
        })()
    }, []);
  return (
    <View style={styles.container}>
      <FlashList data={notes} numColumns={2} estimatedItemSize={100} renderItem={({item}) => (
        <View style={styles.noteView}>
            <Text style={styles.noteTitle}>{item.title}</Text>
            <Text style={styles.noteContent}>{item.note}</Text>
          </View>
      )} />
      <Button title='Add Notes' onPress={() => navigation.navigate('NoteAdd')}></Button>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#c9f5d9',
    marginBottom: 10
  },
  noteView: {
    flex: 1,
    backgroundColor: '#fff',
    margin: 10,
    padding: 10,
    borderRadius: 10,
    shadowColor: 'red',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 7,
    alignItems: 'center'
  },
  noteTitle: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  noteContent: {
    fontSize: 16,
    marginTop: 5
  }
})

export default Home