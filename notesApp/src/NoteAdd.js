import { View, Text, Keyboard, StyleSheet, TextInput, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import app from '../config'
import { collection, addDoc } from "firebase/firestore"; 
import { getFirestore } from "firebase/firestore";

const db = getFirestore(app);

const NoteAdd = () => {
  const [title, setTitle] = useState('')
  const [note, setNote] = useState('')

  const handleAdd = async () => {
    //Add to db
    try {
      const docRef = await addDoc(collection(db, "notes"), {
        title, note
      });
      console.log("Document written with ID: ", docRef.id);
    } catch (e) {
      console.error("Error adding document: ", e);
    }
    setTitle('')
    setNote('')
    Keyboard.dismiss()
  }
  return (
    <View style={style.container}>
      <TextInput 
        placeholder='Title'
        value={title}
        onChangeText={(text) => setTitle(text)}
        style={style.inputTitle}
      />
        <TextInput 
        placeholder='Note'
        value={note}
        onChangeText={(text) => setNote(text)}
        style={style.inputContent}
      />
      <TouchableOpacity
        style={style.button}
        onPress={handleAdd}
      >
        <Text style={style.buttonText}>Add</Text>
      </TouchableOpacity>
    </View>
  )
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#c9f5d9'
  },
  inputTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 18,
    marginBottom: 10,
    height: 50,
    width: '97%',
    borderBottomWidth: 1/2,
    borderLeftWidth: 1/2,
    padding: 10
  },
  inputContent: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 10,
    height: 200,
    width: '97%',
    borderBottomWidth: 1/2,
    borderLeftWidth: 1/2,
    padding: 10
  },
  button: {
    backgroundColor: 'red',
    borderRadius: 10,
    marginTop: 20,
    height: 55,
    width: 150,
    alignItems: 'center', 
    justifyContent: 'center',
    elevation: 7,
    shadowColor: 'blue',
  },
  buttonText: {
    color: 'white',
    fontSize: 22,
    fontWeight: 'bold',
  }
})

export default NoteAdd